# Fall-Merge 
Ein Python-Script das in einer Excel Datei, zwei Identische Fallnummer in 
einer Zeile zusammenfasst.

## Installation von Abhängigkeiten
Das Script hat folgende Abhängigkeiten:

- Python Version 3.10
- openpyxl
- pandas

Python kann über [https://www.python.org/downloads/](Der Python Website)
heruntergeladen werden.
Die restlichen Abhängigkeiten werden danach als Python-Bibliotheken heruntergeladen.

Wenn Python bei der Installation zum PATH hinzugefügt worden ist:
`pip install -r requirements.txt`
(Im selben Verzeichnis ausführen wie das Script und die hinterlegte requirements.txt)

## Ausführung des Scripts
Das Script wird vom selben Verzeichnis aus, einfach via Dateiname und voraus 
gestellten Python-Aufruf gestartet:

`python fall_merge.py`

## Funktion des Script
Das Script erwartet eine `fall_tabelle.xlsx` wo jede Fall einzeln einer Reihe
steht. Jeder Fall existiert zwei mal (=> 2 Werte). Das Script erkennt den Min-
und Max-Wert je Fallnummer und fasst diese in einer Zeile zusammen. Anschließend
löscht das Script die redundant doppelte Einträge und schreibt das ganze in
`ergebnis.xlsx`.


