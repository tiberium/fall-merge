#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Die ersten zwei Zeilen sind 'Best Practice', um einmal sicherzustellen
# das auf *nix Systeme python3 und nicht 2 geladen wird und einmal das alles
# mit dem Zeichensatz utf-8 ausgeführt wird.

# Wir laden die Pandas Bibliothek für das arbeiten mit 'DataFrames'
import pandas as pd

# Definiere die Datei mit den Fällen
working_excel = 'fall_tabelle.xlsx'

# Die Pandas Bibiliothek öffnet die Excel-Datei - Wir brauchen die 
# openpyxl Engine, weil padas xldr Engine ab v2 keine moderne Excel-Datei
# mehr öffnet.
pd_working = pd.ExcelFile(working_excel, engine='openpyxl')

# Wir wandeln die einzelne Tabelle in ein DataFrame um
df = pd.read_excel(pd_working)

# Gruppiere das DataFrame um die Fallnummern
group = df.groupby('Fallnummer')

# Wir definieren ein DataFrame nur für die minimal Werte per Gruppe
df_min = group.min()
print("Fallnummern gruppiert")

# Wir bennen die Spalten um, für das Zusammenführen, danach führen wir
# die beiden DataFrames zusammen, orentiert an der Fallnummer
df_min = df_min.rename(columns={'Wert':'Min-Wert', 'Datum':'Min-Datum'})
df = pd.merge(df, df_min, left_on='Fallnummer', right_index=True,)
print("Minimalwerte hinzugefügt")

# Wir definieren ein DataFrame nur für die maximmal Werte per Gruppe
df_max = group.max()

# Wir bennen die Spalten um, für das Zusammenführen, danach führen wir
# die beiden DataFrames zusammen, orentiert an der Fallnummer
df_max = df_max.rename(columns={'Wert':'Max-Wert', 'Datum':'Max-Datum'})
df = pd.merge(df, df_max, left_on='Fallnummer', right_index=True,)
print("Maximalwerte hinzugefügt")

# Wir löschen alle Duplikate raus, seit wir alle Werte in eine Zeile haben
df = df.drop_duplicates(subset='Fallnummer', keep="first")
print("Doppelte Fallnummern gelöscht")

# Schreibe die Datei - als neue Excel-Datei für ggf. Weiterverarbeitung
df.to_excel('ergebnis.xlsx',encoding='utf-8', index=False)
print("ergebnis.xlsx geschrieben")
